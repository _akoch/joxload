<?php
require_once 'google/appengine/api/cloud_storage/CloudStorageTools.php';
use google\appengine\api\cloud_storage\CloudStorageTools;

$options = [ 'gs_bucket_name' => 'joxload' ];
$upload_url = CloudStorageTools::createUploadUrl('/v2/upload', $options);

echo $upload_url;
