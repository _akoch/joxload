<?php
use google\appengine\api\cloud_storage\CloudStorageTools;

$name = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$name = str_replace('/serve/', '', $name);
$url = CloudStorageTools::getImageServingUrl('gs://joxload/' . $name,
                              ['size' => 1600, 'crop' => false]);

header('Location: ' . str_replace('s1600', 's1920', $url));
